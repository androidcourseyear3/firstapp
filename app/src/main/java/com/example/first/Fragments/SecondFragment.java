package com.example.first.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.first.R;
import com.example.first.interfaces.FragmentCommunication;
import com.example.first.models.Movie;
import com.example.first.singleton.VolleyConfigSingleton;


public class SecondFragment extends Fragment {
    private FragmentCommunication fragmentCommunication;
    private Movie movie;

    public SecondFragment(Movie movie) {
        this.movie = movie;
    }

    public static SecondFragment newInstance(Movie user) {

        SecondFragment fragment = new SecondFragment(user);
        Bundle args = new Bundle();


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);

        TextView title=view.findViewById(R.id.title);
        title.setText(movie.getTitle().toString());
        TextView description=view.findViewById(R.id.description);
        description.setText(movie.getDescription().toString());
        ImageView imageView=view.findViewById(R.id.img_big);
        String imageUrl = movie.getPhoto().toString();
        ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imageView.getContext().getApplicationContext()).getImageLoader();

        imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }

            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                imageView.setImageBitmap(response.getBitmap());
            }

        });

        Button btn=view.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentCommunication != null) {
                    fragmentCommunication.addThirdFragment(movie);
                    Toast.makeText(getContext(), "Go fragment 3", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCommunication) {
            fragmentCommunication = (FragmentCommunication) context;
        }
    }
}
