package com.example.first.Fragments;

import static com.example.first.constants.Constants.AGE;
import static com.example.first.constants.Constants.DESCRIPTION;
import static com.example.first.constants.Constants.ID;
import static com.example.first.constants.Constants.PHOTO;
import static com.example.first.constants.Constants.SCORE;
import static com.example.first.constants.Constants.TITLE;
import static com.example.first.constants.Constants.TYPE;
import static com.example.first.constants.Constants.URL;
import static com.example.first.constants.Constants.YEAR;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.first.Adapters.MovieAdapter;
import com.example.first.R;
import com.example.first.interfaces.FragmentCommunication;
import com.example.first.interfaces.OnItemClick;
import com.example.first.models.Movie;
import com.example.first.singleton.VolleyConfigSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FirstFragment extends Fragment {
    private FragmentCommunication fragmentCommunication;

    ArrayList<Movie> movies = new ArrayList<Movie>();

    MovieAdapter movieAdapter = new MovieAdapter(movies, new OnItemClick() {
        @Override
        public void itemClick(Movie movie) {
            if (fragmentCommunication != null) {
                fragmentCommunication.addSecondFragment(movie);
                Toast.makeText(getContext(), "Go fragment 2", Toast.LENGTH_SHORT).show();
            }
        }

    });

    public static FirstFragment newInstance() {

        Bundle args = new Bundle();

        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMovies();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_first, container, false);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        RecyclerView movieList = (RecyclerView) view.findViewById(R.id.movie_list);
        movieList.setLayoutManager(linearLayoutManager);
        movieList.setAdapter(movieAdapter);

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCommunication) {
            fragmentCommunication = (FragmentCommunication) context;
        }
    }

    public void getMovies() {

        VolleyConfigSingleton volleySingleton=VolleyConfigSingleton.getInstance(getContext());

        RequestQueue queue=volleySingleton.getRequestQueue();

        //String url = BASE_URL + "/";
        String url = "https://my-json-server.typicode.com/Beatrice-Bartos/Demo-Json/movies";

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        handleMovieResponse(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Toast.makeText(getContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
        queue.add(stringRequest);
    }

    public void handleMovieResponse(String response) {

        try {
            JSONArray userJsonArray = new JSONArray(response);
            for (int index = 0; index < userJsonArray.length(); index++) {
                JSONObject userJsonObject = (JSONObject) userJsonArray.get(index);
                if (userJsonArray != null) {

                    String id = userJsonObject.getString(ID);
                    String name = userJsonObject.getString(TITLE);
                    String description = userJsonObject.getString(DESCRIPTION);
                    String url = userJsonObject.getString(URL);
                    String score = userJsonObject.getString(SCORE);
                    String age = userJsonObject.getString(AGE);
                    String year = userJsonObject.getString(YEAR);
                    String photo = userJsonObject.getString(PHOTO);
                    String type = userJsonObject.getString(TYPE);

                    Movie movie = new Movie(id, name, description,url,year,score,age,photo,type);
                    movies.add(movie);

                }


            }
            movieAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
