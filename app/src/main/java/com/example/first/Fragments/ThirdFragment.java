package com.example.first.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.first.R;
import com.example.first.interfaces.FragmentCommunication;
import com.example.first.models.Movie;


public class ThirdFragment extends Fragment {
    private FragmentCommunication fragmentCommunication;
    private Movie movie;

    public ThirdFragment(Movie movie) {
        this.movie = movie;
    }

    public static ThirdFragment newInstance(Movie movie) {

        ThirdFragment fragment = new ThirdFragment(movie);
        Bundle args = new Bundle();


        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_third, container, false);

        TextView score=view.findViewById(R.id.score);
        score.setText(movie.getScore().toString());
        TextView year=view.findViewById(R.id.year);
        year.setText(movie.getYear().toString());
        TextView age=view.findViewById(R.id.age);
        age.setText(movie.getAge().toString());
        TextView type=view.findViewById(R.id.type);
        type.setText(movie.getType().toString());

        Button btn=view.findViewById(R.id.button);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentCommunication != null) {
                    fragmentCommunication.addFourFragment(movie);
                    Toast.makeText(getContext(), "Go fragment 4", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof FragmentCommunication) {
            fragmentCommunication = (FragmentCommunication) context;
        }
    }
}
