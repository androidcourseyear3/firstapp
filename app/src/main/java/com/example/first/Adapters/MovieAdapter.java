package com.example.first.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.example.first.R;
import com.example.first.interfaces.OnItemClick;
import com.example.first.models.Movie;
import com.example.first.singleton.VolleyConfigSingleton;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieViewHolder> {
    private ArrayList<Movie> movies;
    private OnItemClick onItemClick;

    public MovieAdapter(ArrayList<Movie> users, OnItemClick onItemClick) {
        this.movies = users;
        this.onItemClick = onItemClick;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater= LayoutInflater.from(parent.getContext());


        View view =inflater.inflate(R.layout.movie_cell_item,parent,false);
        MovieViewHolder movieViewHolder=new MovieViewHolder(view);

        return movieViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {

        Movie movie=(Movie) movies.get(position);
        ((MovieViewHolder)holder).bind(movie);
    }

    @Override
    public int getItemCount() {
        return this.movies.size();
    }

    class MovieViewHolder extends RecyclerView.ViewHolder{

        private TextView title;
        private TextView score;
        private ImageView imgView;
        private View view;

        public MovieViewHolder(View view)
        {
            super(view);
            imgView=view.findViewById(R.id.img_movie);
            title=view.findViewById(R.id.title);
            score=view.findViewById(R.id.score);

            this.view=view;
        }

        void bind(Movie userObj)
        {
            title.setText(userObj.getTitle());
            score.setText("Appreciate: "+userObj.getScore());
            String imageUrl = userObj.getPhoto().toString();
            ImageLoader imageLoader = VolleyConfigSingleton.getInstance(imgView.getContext().getApplicationContext()).getImageLoader();

            imageLoader.get(imageUrl, new ImageLoader.ImageListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }

                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    imgView.setImageBitmap(response.getBitmap());
                }

            });

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(onItemClick!=null)
                    {
                        onItemClick.itemClick(userObj);//userObj

                    }
                }
            });

        }
    }
}
