package com.example.first.models;

public class Movie {
    public String id;
    public String title;
    public String description;
    public String url;
    public String year;
    public String score;
    public String age;
    public String photo;
    public String type;

    public Movie() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Movie(String id, String title, String description, String url, String year, String score, String age, String photo, String type) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.url = url;
        this.year = year;
        this.score = score;
        this.age = age;
        this.photo = photo;
        this.type = type;
    }


}
