package com.example.first;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.example.first.Fragments.FirstFragment;
import com.example.first.Fragments.FourFragment;
import com.example.first.Fragments.SecondFragment;
import com.example.first.Fragments.ThirdFragment;
import com.example.first.interfaces.FragmentCommunication;
import com.example.first.models.Movie;

public class MainActivity extends AppCompatActivity implements FragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addFirstFragment();
    }

    @Override
    public void addFirstFragment() {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "FirstFragment";
        FragmentTransaction addTransaction= transaction.add(R.id.frame_layout,new FirstFragment(),tag);
        addTransaction.commit();
    }

    @Override
    public void addSecondFragment(Movie movie) {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "SecondFragment";
        FragmentTransaction addTransaction= transaction.replace(R.id.frame_layout, SecondFragment.newInstance(movie),tag);

        addTransaction.addToBackStack(tag);

        addTransaction.commit();
    }

    @Override
    public void addThirdFragment(Movie movie) {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "ThirdFragment";
        FragmentTransaction addTransaction= transaction.replace(R.id.frame_layout, ThirdFragment.newInstance(movie),tag);

        addTransaction.addToBackStack(tag);

        addTransaction.commit();
    }

    @Override
    public void addFourFragment(Movie movie) {
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction= fragmentManager.beginTransaction();
        String tag= "FourFragment";
        FragmentTransaction addTransaction= transaction.replace(R.id.frame_layout, FourFragment.newInstance(movie),tag);

        addTransaction.addToBackStack(tag);

        addTransaction.commit();
    }
}