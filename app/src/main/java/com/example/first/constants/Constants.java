package com.example.first.constants;

public class Constants {
    public static  String BASE_URL="https://jsonplaceholder.typicode.com";
    public static String ID="id";
    public static String TITLE="title";
    public static String DESCRIPTION="description";
    public static String URL="url";
    public static String YEAR="year";
    public static String SCORE="score";
    public static String AGE="age";
    public static String PHOTO="photo";
    public static String TYPE="type";
}
