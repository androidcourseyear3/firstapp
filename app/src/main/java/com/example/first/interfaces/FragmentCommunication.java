package com.example.first.interfaces;

import com.example.first.models.Movie;

public interface FragmentCommunication {
    void addFirstFragment();
    void addSecondFragment(Movie movie);
    void addThirdFragment(Movie movie);
    void addFourFragment(Movie movie);
}
