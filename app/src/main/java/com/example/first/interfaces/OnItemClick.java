package com.example.first.interfaces;

import com.example.first.models.Movie;

public interface OnItemClick {
    public void itemClick(Movie movie);
}
