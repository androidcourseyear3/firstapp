Lista cu filme:

1) Pentru partea de design am ales sa folosesc o paleta de culori in nuante de violet.
Toate fragmentele contin elemente ce folosesc culorile din aceasta paleta.
Pentru aplicatia am avut nevoie si de butoane.Am creat 2 fisiere xml in resurse pentru o putea crea 2 tipuri de butoane cu colturi rotunjite si cu culori de fundal diferite, 
apoi am creat un selector xml unde am introdus si unit cele 2 butoane custom astfel formand background-ul dorit pentru toate butoanele din aplicatie.

2) 
-Aplicatia contine o activitate si 4 fragmente pe care le adaug si le inlocuiesc in acelasi frame Layout.
-Elemntele listei sunt luate dintr-un fisier json.
-Am creat Modelul, interfata onItemClick si clasa Adapter pentru popularea recyclerView-ului.
-M-am folosit de libraria Volley si am creat o clasa VolleyConfigSingleton pentru a putea avea o singura instanta a listei de filme.In interiorul acestei clase am introdus
si imageLoader-ul pe care il folosesc pentru a putea afisa poze in fiecare celula.
-Datele pentru lista de filme le iau din https://my-json-server.typicode.com/Beatrice-Bartos/Demo-Json , un "server" creat de mine urmand indicatiile de pe site-ul https://my-json-server.typicode.com/.
-Pentru a putea lua datele din serverul online creat de mine a fost nevoie sa adaug permisiunea la internet in fisierul manifest al aplicatiei.
-Pentru comunicarea intre fagmente am folosit o interfata FragmentCommunication cu metodele necesare fiecarui fragment.